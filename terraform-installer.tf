provider "openstack" {
  cloud = "ostester"
  version = "~> 1.1"
}

resource "openstack_compute_instance_v2" "zxiiro-test" {
  name      = "zxiiro-test"
  image_name  = "Ubuntu 18.04 LTS (x86_64) [2018-05-09]"
  flavor_name = "v1-standard-4"
  key_pair  = "zxiiro"

  network {
    name = "public"
  }

  user_data = "${data.template_file.gitlab-userdata.rendered}"
}

data "template_file" "gitlab-userdata" {
  template = "${file("${path.module}/gitlab-userdata.tpl")}"
}

