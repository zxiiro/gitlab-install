#!/bin/bash
set -eux -o pipefail

MAIL_NAME="${1:-localhost}"
MAIL_TYPE="${2:-Internet Site}"

sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates

sudo debconf-set-selections <<< "postfix postfix/mailname string $MAIL_NAME"
sudo debconf-set-selections <<< "postfix postfix/main_mailer_type string '$MAIL_TYPE'"
sudo apt-get install -y postfix

curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
sudo EXTERNAL_URL="http://$MAIL_NAME" apt-get install gitlab-ee

